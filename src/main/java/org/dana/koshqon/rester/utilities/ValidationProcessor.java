package org.dana.koshqon.rester.utilities;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class ValidationProcessor {
    private static ValidationProcessor instance;
    private final Validator validator;

    private ValidationProcessor() {
        try (ValidatorFactory f = Validation.buildDefaultValidatorFactory()) {
            this.validator = f.getValidator();
        }
    }

    public static ValidationProcessor getInstance() {
        if (instance == null) {
            instance = new ValidationProcessor();
        }
        return instance;
    }

    public Validator getValidator() {
        return validator;
    }

}
