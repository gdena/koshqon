package org.dana.koshqon.rester.utilities;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.validation.ConstraintViolation;
import java.time.OffsetDateTime;
import java.util.Set;

public class Json {

    private static Json instance;
    private final Gson gson;

    public Json() {
        this.gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(OffsetDateTime.class, new OffsetDateTimeConverter())
                .serializeNulls()
                .create();
    }

    public static Json instance() {
        if (instance == null) {
            instance = new Json();
        }
        return instance;
    }

    public static String marshal(Object object) {
        return Json.instance().gson().toJson(object);
    }

    public static <T> T unmarshal(String json, Class<T> type) throws ValidationException {
        T result = Json.instance().gson().fromJson(json, type);
        Set<ConstraintViolation<T>> violations = ValidationProcessor.getInstance().getValidator().validate(result);
        if (!violations.isEmpty()) {
            StringBuilder messageBuilder = new StringBuilder();
            for (ConstraintViolation<T> violation : violations) {
                messageBuilder.append(violation.getMessage());
                messageBuilder.append("; ");
            }
            throw new ValidationException(messageBuilder.toString());
        }
        return result;
    }

    public static String marshalError(String message) {
        return Json.marshal(new Error(message));
    }

    public Gson gson() {
        return gson;
    }

    private record Error(String error) {
    }
}
