package org.dana.koshqon.rester.utilities;

public class HexadecimalConverter {
    public static String encode(byte[] hash) {
        StringBuilder sb = new StringBuilder();
        for (byte b : hash) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
}
