package org.dana.koshqon.rester.utilities;


import org.dana.koshqon.rester.logger.LogWriter;

public class ErrorHandler {
    public static void logError(Throwable err) {
        LogWriter.getInstance().write(err.getMessage(), 5);
        System.out.println(err.getMessage());
        err.printStackTrace();
    }
}
