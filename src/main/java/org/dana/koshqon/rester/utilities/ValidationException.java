package org.dana.koshqon.rester.utilities;

public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}
