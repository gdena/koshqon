package org.dana.koshqon.rester.utilities;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.time.OffsetDateTime;

public class Json2 {

    private static Json2 instance;
    private final Gson gson;

    public Json2() {
        this.gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .registerTypeAdapter(OffsetDateTime.class, new OffsetDateTimeConverter())
                .serializeNulls()
                .create();
    }

    public static Json2 instance() {
        if (instance == null) {
            instance = new Json2();
        }
        return instance;
    }

    public static String marshal(Object object) {
        return Json2.instance().gson().toJson(object);
    }

    public static <T> T unmarshal(String json, Class<T> type) {
        return Json2.instance().gson().fromJson(json, type);
    }

    public static String marshalError(String message) {
        return Json2.marshal(new Error(message));
    }

    public Gson gson() {
        return gson;
    }

    private record Error(String error) {
    }
}

