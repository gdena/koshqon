package org.dana.koshqon.rester;

import com.sun.net.httpserver.HttpServer;
import org.dana.koshqon.rester.routing.Router;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Server implements Runnable {
    int port;
    Router router;
    int threadPoolSize = 10;
    int maxThreadPoolSize = 100;

    public Server(int port, Router router) {
        this.port = port;
        this.router = router;
    }

    public void run() {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        executor.setCorePoolSize(threadPoolSize);
        executor.setMaximumPoolSize(maxThreadPoolSize);
        executor.setKeepAliveTime(60, TimeUnit.SECONDS);
        HttpServer server;
        try {
            server = HttpServer.create(new InetSocketAddress(port), 0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        server.setExecutor(executor);
        router.build(server);
        server.start();
    }
}
