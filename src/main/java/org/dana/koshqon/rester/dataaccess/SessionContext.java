package org.dana.koshqon.rester.dataaccess;

import org.hibernate.Session;

class SessionContext {
    private String id;
    private Session session;

    public SessionContext(String id, Session session) {
        this.id = id;
        this.session = session;
    }

    public String getId() {
        return id;
    }

    public SessionContext setId(String id) {
        this.id = id;
        return this;
    }

    public Session getSession() {
        return session;
    }

    public SessionContext setSession(Session session) {
        this.session = session;
        return this;
    }
}
