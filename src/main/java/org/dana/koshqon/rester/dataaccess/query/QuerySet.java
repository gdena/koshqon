package org.dana.koshqon.rester.dataaccess.query;

import org.hibernate.Session;

public class QuerySet {
    private Session session;

    public QuerySet(Session session) {
        this.session = session;
    }

    public <T> Select<T> select(Class<T> type) {
        return new Select<T>(type, session);
    }

    public <T> void insert(T entity) {
        session.beginTransaction();
        try {
            session.persist(entity);
            session.getTransaction().commit();
        } catch (Exception err) {
            session.getTransaction().rollback();
            throw err;
        }
    }

    public <T> void merge(T entity) {
        session.beginTransaction();
        try {
            session.merge(entity);
            session.getTransaction().commit();
        } catch (Exception err) {
            session.getTransaction().rollback();
            throw err;
        }
    }

    public <T> void transactionalInsert(T entity) {
        session.persist(entity);
    }

    public <T> void transactionalMerge(T entity) {
        session.merge(entity);
    }


}
