package org.dana.koshqon.rester.dataaccess.query;

record ValueContainer(String param, String operator, Object value) {
}
