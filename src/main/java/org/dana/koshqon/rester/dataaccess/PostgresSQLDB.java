package org.dana.koshqon.rester.dataaccess;

import org.dana.koshqon.rester.dataaccess.query.QuerySet;
import org.dana.koshqon.rester.routing.RequestContext;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public final class PostgresSQLDB {
    private final Configuration cfg;
    private SessionFactory factory;
    private final ThreadLocal<SessionContext> sessionContainer = ThreadLocal.withInitial(() -> createSessionContext(RequestContext.getUid()));

    public PostgresSQLDB(String url, String username, String password) {
        cfg = new Configuration();
        cfg.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
        cfg.setProperty("hibernate.connection.url", url);
        cfg.setProperty("hibernate.connection.username", username);
        cfg.setProperty("hibernate.connection.password", password);
        cfg.setProperty("hibernate.c3p0.min_size", "10");
        cfg.setProperty("hibernate.c3p0.max_size", "100");
        cfg.setProperty("hibernate.c3p0.acquire_increment", "5");
        cfg.setProperty("hibernate.c3p0.timeout", "30");
        cfg.setProperty("hibernate.show_sql", "false");
        cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
    }

    public PostgresSQLDB addAnnotatedClass(Class[] cls) {
        for (Class e : cls) {
            cfg.addAnnotatedClass(e);
        }
        this.factory = cfg.buildSessionFactory();
        return this;
    }

    public QuerySet querySet() {
        return new QuerySet(this.getSession());
    }

    private SessionContext createSessionContext(String uid) {
        return new SessionContext(uid, factory.openSession());
    }

    public Session getSession() {
        SessionContext sessionContext = sessionContainer.get();
        if (!sessionContext.getId().equals(RequestContext.getUid())) {
            sessionContext.getSession().close();
            sessionContext.setSession(null);
        }
        Session session = sessionContext.getSession();
        if (session == null || !session.isOpen()) {
            session = factory.openSession();
            sessionContext.setId(RequestContext.getUid()).setSession(session);
        }
        return sessionContext.getSession();
    }
}