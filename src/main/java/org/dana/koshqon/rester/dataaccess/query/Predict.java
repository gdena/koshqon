package org.dana.koshqon.rester.dataaccess.query;

record Predict(String queryString, Object... args) {
}
