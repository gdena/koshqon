package org.dana.koshqon.rester.dataaccess.query;

import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class Select<T> {
    private final Session session;
    private final Class<T> type;
    List<ValueContainer> queryParams;
    private Predict predict = null;
    private String order = null;


    public Select(Class<T> entity, Session session) {
        this.type = entity;
        this.queryParams = new ArrayList<ValueContainer>();
        this.session = session;
    }

    public Select<T> equal(String paramName, Object value) {
        queryParams.add(new ValueContainer(paramName, "=", value));
        return this;
    }

    public Select<T> greater(String paramName, Object value) {
        queryParams.add(new ValueContainer(paramName, ">", value));
        return this;
    }

    public Select<T> less(String paramName, Object value) {
        queryParams.add(new ValueContainer(paramName, "<", value));
        return this;
    }

    public Select<T> notEqual(String paramName, Object value) {
        queryParams.add(new ValueContainer(paramName, "<>", value));
        return this;
    }

    public Select<T> in(String paramName, Object value) {
        queryParams.add(new ValueContainer(paramName, "IN", value));
        return this;
    }

    public Select<T> notIn(String paramName, Object value) {
        queryParams.add(new ValueContainer(paramName, "NOT IN", value));
        return this;
    }

    public Select<T> is(String paramName, Object value) {
        queryParams.add(new ValueContainer(paramName, "IS", value));
        return this;
    }

    public Select<T> isNot(String paramName, Object value) {
        queryParams.add(new ValueContainer(paramName, "IS NOT", value));
        return this;
    }

    private String constructQuery() {
        StringBuilder query = new StringBuilder(String.format("FROM %s o", this.type.getSimpleName()));
        for (int i = 0; i < queryParams.size(); i++) {
            if (i == 0) {
                query.append(" WHERE ");
            } else {
                query.append(" AND ");
            }
            ValueContainer param = queryParams.get(i);
            query.append(String.format("o.%s %s :%s", param.param(), param.operator(), param.param().replaceAll("[^a-zA-Z0-9]", "")));
        }
        return query.toString();

    }

    private String constructPredictQuery() {
        StringBuilder query = new StringBuilder(String.format("FROM %s WHERE ", this.type.getSimpleName()));
        if (this.predict != null) {
            query.append(predict.queryString());
        }
        for (int i = 0; i < queryParams.size(); i++) {
            if (i > 0 || this.predict != null) {
                query.append(" AND ");
            }
            ValueContainer param = queryParams.get(i);
            query.append(String.format("%s %s :%s", param.param(), param.operator(), param.param().replaceAll("[^a-zA-Z0-9]", "")));
        }
        if (order != null) {
            query.append(" ORDER BY ").append(order);
        }
        return query.toString();

    }

    public Query<T> build() {
        String constructedString = constructPredictQuery();
        Query<T> query = session.createQuery(constructedString, this.type);
        if (predict != null) {
            int i = 1;
            for (Object arg : predict.args()) {
                query.setParameter(i, arg);
                i++;
            }
        }
        for (ValueContainer param : this.queryParams) {
            query.setParameter(param.param().replaceAll("[^a-zA-Z0-9]", ""), param.value());
        }
        return query;
    }

    public Query<T> custom(String queryString) {
        String query = String.format("FROM %s", this.type.getSimpleName()) + " " + queryString;
        return session.createQuery(query, this.type);
    }

    private Query<T> where2(String queryString, Object... args) {
        String query = String.format("FROM %s", this.type.getSimpleName()) + " WHERE " + queryString;
        Query<T> resultQuery = session.createQuery(query, this.type);
        int i = 1;
        for (Object arg : args) {
            resultQuery.setParameter(i, arg);
            i++;
        }
        return resultQuery;
    }

    public Select<T> where(String queryString, Object... args) throws QuerysetConstructorException {
        if (this.predict == null) {
            this.predict = new Predict(queryString, args);
        } else {
            throw new QuerysetConstructorException();
        }
        return this;
    }

    public Select<T> orderBy(String param) throws QuerysetConstructorException {
        if (order == null) {
            this.order = param;
        } else {
            throw new QuerysetConstructorException();
        }
        return this;
    }
}
