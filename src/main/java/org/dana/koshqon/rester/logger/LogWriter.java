package org.dana.koshqon.rester.logger;

import org.dana.koshqon.rester.utilities.Json;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class LogWriter implements Runnable {
    private static LogWriter instance;
    private final String token;
    private final URI uri;
    private final String appName;
    private final BlockingQueue<Log> logQueue;

    private LogWriter() {
        this.token = "Basic " + System.getenv("LOG_TOKEN");
        this.uri = URI.create(System.getenv("LOG_URI"));
        this.appName = System.getenv("APP_NAME");
        this.logQueue = new LinkedBlockingQueue<>(200);
    }

    public static LogWriter getInstance() {
        if (instance == null) {
            instance = new LogWriter();
        }
        return instance;
    }

    private void send(Log log) throws IOException, InterruptedException {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(uri)
                .header("Content-Type", "application/json")
                .header("Authorization", token)
                .method("POST", HttpRequest.BodyPublishers.ofString(Json.marshal(log)))
                .build();
        HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
    }

    public void write(String logData, int logLevel) {
        try {
            logQueue.put(new Log(appName, logLevel, logData));
        } catch (InterruptedException ignored) {
        }
    }

    @Override
    public void run() {
        System.out.println("Logger started");
        while (true) {
            try {
                this.send(logQueue.take());
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
