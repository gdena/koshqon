package org.dana.koshqon.rester.logger;

public record Log(
        String applicationName,
        int logLevel,
        String logData
) {
}
