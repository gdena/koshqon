package org.dana.koshqon.rester;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.dana.koshqon.rester.http.ContentTypes;
import org.dana.koshqon.rester.http.HttpHeaders;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.logger.LogWriter;
import org.dana.koshqon.rester.response.models.Response;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.routing.ExchangeLog;
import org.dana.koshqon.rester.utilities.ErrorHandler;
import org.dana.koshqon.rester.utilities.Json;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public abstract class HttpController implements HttpHandler {
    protected HttpExchange exchange;
    protected Response response;
    protected byte[] requestBody;
    private Map<String, String> queryParams;

    private static String convertStreamToString(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }
        bufferedReader.close();
        return stringBuilder.toString();
    }

    public final void handle(HttpExchange exchange) {
        this.exchange = exchange;
        this.response = new Response();
        try {
            queryToMap(exchange.getRequestURI().getQuery());
        } catch (Exception ignored) {
        }
        try {
            String method = exchange.getRequestMethod();
            switch (method) {
                case "GET" -> doGet();
                case "POST" -> doPost();
                case "PUT" -> doPut();
                case "PATCH" -> doPatch();
                case "DELETE" -> doDelete();
                case "OPTIONS" -> doOptions();
                default -> handleDefaultAnswer();
            }
        } catch (ResponseReturned ignored) {
        } catch (Exception err) {
            LogWriter.getInstance().write(err.getMessage(), 5);
            internalServerErrorAnswer(err);
        } finally {
            try {
                if (!exchange.getRequestURI().getPath().equals("/ping")) {
                    LogWriter.getInstance().write(Json.marshal(ExchangeLog.fromExchange(exchange, requestBody, response.getBody())), 1);
                }
            } catch (Exception err) {
                ErrorHandler.logError(err);
            }
            this.exchange = null;
            this.response = null;
            this.requestBody = null;
        }
    }

    @Nullable
    protected String getQueryParam(String key) {
        try {
            if (queryParams == null) {
                return null;
            }
            if (queryParams.containsKey(key)) {
                return queryParams.get(key);
            }
            return null;
        } catch (Exception err) {
            return null;
        }
    }

    public Map<String, String> extractFormData() throws IOException {
        Map<String, String> formData = new HashMap<>();

        if ("POST".equals(exchange.getRequestMethod())) {
            InputStream requestBody = exchange.getRequestBody();
            String requestBodyString = convertStreamToString(requestBody).replaceAll("\n", "");

            String[] parameters = requestBodyString.split("&");
            for (String parameter : parameters) {
                String[] keyValue = parameter.split("=");
                if (keyValue.length == 2) {
                    String key = keyValue[0];
                    String value = keyValue[1];
                    formData.put(key, value);
                }
            }
        }

        return formData;
    }

    protected void doGet() throws Exception {
        handleDefaultAnswer();
    }

    protected void doPost() throws Exception {
        handleDefaultAnswer();
    }

    protected void doDelete() throws Exception {
        handleDefaultAnswer();
    }

    protected void doPatch() throws Exception {
        handleDefaultAnswer();
    }

    protected void doPut() throws Exception {
        handleDefaultAnswer();
    }

    protected void doOptions() throws Exception {
        exchange.getResponseHeaders().set("Access-Control-Allow-Origin", "*");
        exchange.getResponseHeaders().set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, PATCH");
        exchange.getResponseHeaders().set("Access-Control-Allow-Headers", "Content-Type, Authorization");
        exchange.getResponseHeaders().set("Access-Control-Max-Age", "3600");
        exchange.sendResponseHeaders(200, -1);
    }

    protected void handleDefaultAnswer() throws ResponseReturned {
        this.returnResponse(HttpStatusCodes.METHOD_NOT_ALLOWED, Json.marshalError("Method Not Allowed"));
    }

    private void internalServerErrorAnswer(Exception error) {
        try {
            returnResponse(HttpStatusCodes.CONFLICT, Json.marshalError(error.getClass().getSimpleName()));
        } catch (Exception err) {
            ErrorHandler.logError(err);
        }
    }

    private void queryToMap(String query) {
        if (query == null) {
            return;
        }
        queryParams = new HashMap<>();
        for (String param : query.split("&")) {
            String[] entry = param.split("=");
            if (entry.length > 1) {
                queryParams.put(entry[0], entry[1]);
            } else {
                queryParams.put(entry[0], "");
            }
        }
    }

    protected final void makeHttpResponse() {
        long contentLength;
        try {
            if (response.getBody() == null || response.getBody().isEmpty()) {
                contentLength = -1;
            } else {
                contentLength = response.getBody().getBytes().length;
            }

            for (String key : response.headers().keySet()) {
                getExchange().getResponseHeaders().add(key, response.headers().get(key));
            }
            exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            exchange.sendResponseHeaders(response.getStatus(), contentLength);
            exchange.getResponseBody().write(response.getBody().getBytes());
        } catch (Exception err) {
            ErrorHandler.logError(err);
        }
    }

    protected final void makeHttpResponse(String contentType, int statusCode, String body) {
        response.headers().put(HttpHeaders.CONTENT_TYPE, contentType);
        response.setStatus(statusCode).setBody(body);
        makeHttpResponse();
    }

    protected final void returnResponse(int statusCode, String body) throws ResponseReturned {
        response.headers().put(HttpHeaders.CONTENT_TYPE, ContentTypes.APPLICATION_JSON);
        response.setStatus(statusCode).setBody(body);
        makeHttpResponse();
        throw new ResponseReturned();
    }


    protected final HttpExchange getExchange() {
        return exchange;
    }

    protected final String getRequestHeader(String key) {
        return exchange.getRequestHeaders().getFirst(key);
    }

    protected final String getResponseHeader(String key) {
        return exchange.getResponseHeaders().getFirst(key);
    }

    protected final byte[] getRequestBody() throws IOException {
        this.requestBody = this.exchange.getRequestBody().readAllBytes();
        return this.requestBody;
    }
}
