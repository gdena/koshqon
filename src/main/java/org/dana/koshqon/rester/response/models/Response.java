package org.dana.koshqon.rester.response.models;

import org.dana.koshqon.rester.http.HttpStatusCodes;

import java.util.HashMap;
import java.util.Map;

public class Response {
    private final Map<String, String> headers;
    private int status;
    private String body;

    public Response() {
        this.status = HttpStatusCodes.OK;
        this.headers = new HashMap<String, String>();
    }

    public int getStatus() {
        return status;
    }

    public Response setStatus(int status) {
        this.status = status;
        return this;
    }

    public String getBody() {
        return body;
    }

    public Response setBody(String body) {
        this.body = body;
        return this;
    }

    public Map<String, String> headers() {
        return headers;
    }
}
