package org.dana.koshqon.rester.routing;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;

public record ExchangeLog(
        String request_id,
        String remoteIp,
        String method,
        String endpoint,
        String requestBody,
        int responseCode,
        String responseBody

) {
    public static ExchangeLog fromExchange(HttpExchange exchange, byte[] requestBody, String respBody) throws IOException {
        String body = "";
        if (requestBody != null) {
            body = new String(requestBody);
        }
        return new ExchangeLog(
                RequestContext.getUid(),
                exchange.getRequestHeaders().getFirst("X-Real-IP"),
                exchange.getRequestMethod(),
                exchange.getRequestURI().getPath(),
                body,
                exchange.getResponseCode(),
                respBody
        );
    }
}
