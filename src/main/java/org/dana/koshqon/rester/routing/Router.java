package org.dana.koshqon.rester.routing;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

import java.util.HashSet;

public class Router {
    private final HashSet<Endpoint> endpoints;

    public Router() {
        this.endpoints = new HashSet<Endpoint>();
    }

    public Router addEndpoint(Endpoint endpoint) {
        this.endpoints.add(endpoint);
        return this;
    }

    public void build(HttpServer server) {
        for (Endpoint endpoint : endpoints) {
            HttpContext context = server.createContext(endpoint.getPath());
            context.setHandler(endpoint);
        }
    }

}
