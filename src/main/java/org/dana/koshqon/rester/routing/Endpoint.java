package org.dana.koshqon.rester.routing;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.dana.koshqon.rester.HttpController;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public final class Endpoint<T extends HttpController> implements HttpHandler {
    private final String path;
    private final Class<T> controllerClass;
    private final Middleware middleware;

    public Endpoint(String path, Class<T> controller, Middleware middleware) {
        this.path = path;
        this.controllerClass = controller;
        this.middleware = middleware;
    }

    private T createController() throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        return controllerClass.getDeclaredConstructor().newInstance();
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        RequestContext.generateUid();
        try {
            if (!exchange.getRequestMethod().equals("OPTIONS")) {
                middleware.handle(exchange);
            }
        } catch (FilterNotPassException e) {
            return;
        }
        HttpHandler controller;
        try {
            controller = this.createController();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                 InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        controller.handle(exchange);
        exchange.getHttpContext().getAttributes().clear();
        exchange.getResponseBody().close();
        exchange.getRequestBody().close();
        exchange.close();
    }

    public String getPath() {
        return path;
    }
}
