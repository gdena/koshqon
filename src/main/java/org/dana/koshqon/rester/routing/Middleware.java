package org.dana.koshqon.rester.routing;

import com.sun.net.httpserver.HttpExchange;

import java.util.LinkedHashSet;

public final class Middleware {
    private LinkedHashSet<Filter> filters;

    public Middleware() {
        this.filters = new LinkedHashSet<Filter>();
    }

    public void handle(HttpExchange exchange) throws FilterNotPassException {
        for (Filter filter : filters) {
            if (!filter.apply(exchange)) {
                throw new FilterNotPassException();
            }
        }
    }

    public LinkedHashSet<Filter> filters() {
        return filters;
    }
}
