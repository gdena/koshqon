package org.dana.koshqon.rester.routing;

import com.sun.net.httpserver.HttpExchange;

public interface Filter {
    boolean apply(HttpExchange exchange);
}
