package org.dana.koshqon.rester.routing;

import java.util.UUID;

public class RequestContext {
    private static final ThreadLocal<String> uid = ThreadLocal.withInitial(() -> UUID.randomUUID().toString());

    public static String getUid() {
        return uid.get();
    }

    public static void generateUid() {
        RequestContext.uid.set(UUID.randomUUID().toString());
    }
}
