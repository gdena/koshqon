package org.dana.koshqon.api.utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    public static final String PHONE = "^\\d{1,15}$";
    public static final String EMAIL = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";

    public static boolean validate(String regexp, String value) {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(value);

        return matcher.matches();
    }
}
