package org.dana.koshqon.api.utilities;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.dana.koshqon.api.models.responses.ProfileResponse;
import org.dana.koshqon.database.entities.User;
import org.dana.koshqon.rester.utilities.Json;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Base64;
import java.util.Date;

public class Jwt {
    private static Jwt instance;
    private Key key;

    private Jwt() {
        byte[] decodedKey = Base64.getDecoder().decode(System.getenv("JWT_KEY"));
        this.key = new SecretKeySpec(decodedKey, 0, decodedKey.length, "HmacSHA256");
    }

    public static Jwt getInstance() {
        if (instance == null) {
            instance = new Jwt();
        }
        return instance;
    }

    public String generateToken(User user) {
        long nowMillis = System.currentTimeMillis(); // Current time in milliseconds
        long expMillis = nowMillis + (24 * 60 * 60 * 1000); // 24 hours in milliseconds
        Date exp = new Date(expMillis); // The expiration time
        String profileDto = Json.marshal(ProfileResponse.fromProfile(user.getProfile()));
        return Jwts.builder()
                .setSubject(user.getId().toString())
                .setIssuedAt(new Date(nowMillis))
                .setExpiration(exp)
                .signWith(key)
                .claim("profile", profileDto)
                .compact();
    }

    public String generateRefreshToken(User user) {
        long nowMillis = System.currentTimeMillis(); // Current time in milliseconds
        long expMillis = nowMillis + (7 * 24 * 60 * 60 * 1000); // 24 hours in milliseconds
        Date exp = new Date(expMillis); // The expiration time
        String profileDto = Json.marshal(ProfileResponse.fromProfile(user.getProfile()));
        return Jwts.builder()
                .setSubject(user.getId().toString())
                .setIssuedAt(new Date(nowMillis))
                .setExpiration(exp)
                .signWith(key)
                .claim("profile", profileDto)
                .compact();
    }

    public Long validateToken(String token) {
        Jws<Claims> claimsJws = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token);

        Claims claims = claimsJws.getBody();

        // Successfully parsed and validated the token. Now you can use the claims.
        return Long.parseLong(claims.getSubject());
    }
}
