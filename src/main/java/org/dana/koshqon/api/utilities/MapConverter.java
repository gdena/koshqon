package org.dana.koshqon.api.utilities;

import java.util.Map;
import java.util.stream.Collectors;

public class MapConverter {
    public static Map<String, Object> convertToStringObjectMap(Map<String, String> stringMap) {
        return stringMap.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> b));
    }
}
