package org.dana.koshqon.api.models.responses;

public record Message(String message) {
}
