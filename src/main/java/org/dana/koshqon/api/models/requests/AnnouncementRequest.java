package org.dana.koshqon.api.models.requests;

import java.util.List;

public record AnnouncementRequest(
        String city,
        String country,
        String address,
        Long announcementTypeId,
        String description,
        List<byte[]> images
) {
}
