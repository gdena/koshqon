package org.dana.koshqon.api.models.responses;

import org.dana.koshqon.database.entities.Announcement;

import java.util.List;

public record AnnouncementDto(
        Long id,
        boolean active,
        String city,
        String country,
        String address,
        String announcementType,
        String description,
        Long profile,
        List<byte[]> images
) {
    public static AnnouncementDto fromAnnouncement(Announcement announcement, List<byte[]> images) {
        return new AnnouncementDto(
                announcement.getId(),
                announcement.isActive(),
                announcement.getCity(),
                announcement.getCountry(),
                announcement.getAddress(),
                announcement.getAnnouncementType().getName(),
                announcement.getDescription(),
                announcement.getProfile().getId(),
                images
        );
    }
}
