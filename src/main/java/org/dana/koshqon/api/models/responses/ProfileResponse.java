package org.dana.koshqon.api.models.responses;

import org.dana.koshqon.database.entities.Profile;

public record ProfileResponse(
        Long id,
        String first_name,
        String last_name,
        String country,
        int age,
        String biography,
        int rating
) {
    public static ProfileResponse fromProfile(Profile profile) {
        return new ProfileResponse(
                profile.getId(),
                profile.getFirstName(),
                profile.getLastName(),
                profile.getCountry(),
                profile.getAge(),
                profile.getBiography(),
                profile.getRating()
        );
    }
}
