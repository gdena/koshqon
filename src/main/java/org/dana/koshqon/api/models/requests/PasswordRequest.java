package org.dana.koshqon.api.models.requests;

import javax.validation.constraints.Size;

public record PasswordRequest(
        String phone,
        @Size(min = 8, max = 30, message = "password must be 8-30")
        String password
) {
}