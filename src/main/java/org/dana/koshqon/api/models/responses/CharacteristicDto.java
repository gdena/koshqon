package org.dana.koshqon.api.models.responses;

import org.dana.koshqon.database.entities.Characteristic;

import java.util.Map;

public record CharacteristicDto(
        Long id,
        Long profile,
        Map<String, Object> aspects,
        Map<String, Object> interests
) {
    public static CharacteristicDto fromCharacteristic(Characteristic characteristic) {
        return new CharacteristicDto(
                characteristic.getId(),
                characteristic.getProfile().getId(),
                characteristic.getAspects(),
                characteristic.getInterests()
        );
    }
}
