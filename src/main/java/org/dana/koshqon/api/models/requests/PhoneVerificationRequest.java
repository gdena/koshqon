package org.dana.koshqon.api.models.requests;

public record PhoneVerificationRequest(
        String phone
) {
}
