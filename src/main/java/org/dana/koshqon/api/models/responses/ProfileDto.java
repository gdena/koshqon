package org.dana.koshqon.api.models.responses;

import org.dana.koshqon.database.entities.Characteristic;
import org.dana.koshqon.database.entities.Profile;

public record ProfileDto(
        Long id,
        String first_name,
        String last_name,
        String country,
        int age,
        String biography,
        int rating,
        byte[] image,
        CharacteristicDto characteristicDto
) {
    public static ProfileDto fromProfile(Profile profile, byte[] image, Characteristic characteristic) {
        return new ProfileDto(
                profile.getId(),
                profile.getFirstName(),
                profile.getLastName(),
                profile.getCountry(),
                profile.getAge(),
                profile.getBiography(),
                profile.getRating(),
                image,
                CharacteristicDto.fromCharacteristic(characteristic)
        );
    }
}
