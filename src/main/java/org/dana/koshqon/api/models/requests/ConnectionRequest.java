package org.dana.koshqon.api.models.requests;

public record ConnectionRequest(
        Long profile_id
) {
}
