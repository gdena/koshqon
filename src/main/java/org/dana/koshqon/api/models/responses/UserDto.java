package org.dana.koshqon.api.models.responses;

import org.dana.koshqon.database.entities.User;

public record UserDto(Long id,
                      String login,
                      String phone,
                      Boolean active,
                      ProfileResponse profile) {
    public static UserDto fromUser(User user) {
        return new UserDto(
                user.getId(),
                user.getLogin(),
                user.getPhone(),
                user.getActive(),
                ProfileResponse.fromProfile(user.getProfile())
        );
    }
}
