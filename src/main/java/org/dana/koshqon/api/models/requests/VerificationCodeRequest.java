package org.dana.koshqon.api.models.requests;

import javax.validation.constraints.Size;

public record VerificationCodeRequest(
        @Size(min = 11, max = 13, message = "Phone in must be in e164 format")
        String phone,
        @Size(min = 6, max = 6, message = "secret code must be exactly 6 characters long")
        String secret
) {
}
