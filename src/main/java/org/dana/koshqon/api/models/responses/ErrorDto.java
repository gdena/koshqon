package org.dana.koshqon.api.models.responses;

import org.dana.koshqon.database.entities.SystemError;

public record ErrorDto(int errorCode, String name, String description) {
    public static ErrorDto fromError(SystemError error) {
        return new ErrorDto(
                error.getErrorCode(),
                error.getName(),
                error.getDescription()
        );
    }
}