package org.dana.koshqon.api.models.responses;

import java.util.Date;

public record TokenDto(
        Long user_id,
        String token,
        String refresh_token,
        Date jwt_expiration_date,
        Date rt_expiration_date
) {
}
