package org.dana.koshqon.api.models.responses;

import org.dana.koshqon.database.entities.Connection;

public record ConnectionDto(
        Long id,
        ProfileResponse profileDto1,
        ProfileResponse profileDto2,
        String status
) {
    public static ConnectionDto fromConnection(Connection connection) {
        return new ConnectionDto(
                connection.getId(),
                ProfileResponse.fromProfile(connection.getProfile1()),
                ProfileResponse.fromProfile(connection.getProfile2()),
                connection.getStatus().getName()
        );
    }
}
