package org.dana.koshqon.api.models.requests;

import java.util.Date;
import java.util.Map;

public record ProfileEditRequest(
        String first_name,
        String last_name,
        Date birthDate,
        String gender,
        String country,
        String biography,
        Map<String, Object> aspects,
        Map<String, Object> interests
) {
}
