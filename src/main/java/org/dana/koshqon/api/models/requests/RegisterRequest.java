package org.dana.koshqon.api.models.requests;

import java.util.Date;

public record RegisterRequest(
        String first_name,
        String last_name,
        Date birthDate,
        String gender,
        String country,
        String password,
        String login,
        String phone
) {
}
