package org.dana.koshqon.api.models.dtos;

import java.time.OffsetDateTime;

public class SecretCode {
    private String phone;
    private String secret;
    private Integer status;
    private OffsetDateTime created_at;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getPhone_number() {
        return phone;
    }

    public void setPhone_number(String phone) {
        this.phone = phone;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public OffsetDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at() {
        this.created_at = OffsetDateTime.now();
    }
}
