package org.dana.koshqon.api.filters;

import com.sun.net.httpserver.HttpExchange;
import org.dana.koshqon.api.utilities.Jwt;
import org.dana.koshqon.database.caches.UserCache;
import org.dana.koshqon.database.entities.User;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.routing.Filter;
import org.dana.koshqon.rester.utilities.ErrorHandler;

public class JwtAuthenticator implements Filter {
    private boolean checkAuthentication(HttpExchange exchange) {
        String authHeader = exchange.getRequestHeaders().getFirst("Authorization");
        exchange.getResponseHeaders().add("Content-Type", "application/json");
        if (authHeader == null) {
            return false;
        }
        String encodedCredentials = authHeader.split(" ")[1];
        Long userId = Jwt.getInstance().validateToken(encodedCredentials);

        User user = UserCache.getInstance().getUser(userId);
        if (user != null && user.getActive()) {
            exchange.getResponseHeaders().add("X-USER-ID", userId.toString());
            return true;
        }
        return false;
    }

    private void unauthorizedResponse(HttpExchange exchange) {
        try {
            String body = "UNAUTHORIZED";
            exchange.getResponseHeaders().set("Access-Control-Allow-Origin", "*");
            exchange.sendResponseHeaders(HttpStatusCodes.UNAUTHORIZED, body.getBytes().length);
            exchange.getResponseBody().write(body.getBytes());
            exchange.getResponseBody().close();
        } catch (Exception err) {
            ErrorHandler.logError(err);
        }
    }

    @Override
    public boolean apply(HttpExchange exchange) {
        try {
            if (checkAuthentication(exchange)) {
                return true;
            }
        } catch (Exception err) {
            ErrorHandler.logError(err);
        }
        unauthorizedResponse(exchange);
        return false;
    }

}
