package org.dana.koshqon.api.routers;


import org.dana.koshqon.api.controllers.*;
import org.dana.koshqon.api.filters.JwtAuthenticator;
import org.dana.koshqon.rester.routing.Endpoint;
import org.dana.koshqon.rester.routing.Middleware;
import org.dana.koshqon.rester.routing.Router;

public class RouterFactory {
    public static Router buildRouter() {
        Middleware defaultMiddleware = new Middleware();
        Router router = new Router();
        Middleware authenticationMiddleware = new Middleware();
        authenticationMiddleware.filters().add(new JwtAuthenticator());

        router.addEndpoint(new Endpoint<>("/ping", PingController.class, defaultMiddleware));
        router.addEndpoint(new Endpoint<>("/auth", AuthController.class, defaultMiddleware));
        router.addEndpoint(new Endpoint<>("/auth/refresh", RefreshAuthController.class, authenticationMiddleware));
        router.addEndpoint(new Endpoint<>("/register", RegisterController.class, defaultMiddleware));
        router.addEndpoint(new Endpoint<>("/v1/password", PasswordController.class, defaultMiddleware));
        router.addEndpoint(new Endpoint<>("/v1/verification", PhoneVerificationController.class, defaultMiddleware));
        router.addEndpoint(new Endpoint<>("/v1/profile", ProfileController.class, authenticationMiddleware));
        router.addEndpoint(new Endpoint<>("/v2/profile", ProfileController.class, defaultMiddleware));
        router.addEndpoint(new Endpoint<>("/v1/announcement", AnnouncementController.class, authenticationMiddleware));
        router.addEndpoint(new Endpoint<>("/v2/announcement", AnnouncementController.class, defaultMiddleware));
        router.addEndpoint(new Endpoint<>("/v1/connection", ConnectionController.class, authenticationMiddleware));
        router.addEndpoint(new Endpoint<>("/v1/favorites", FavoritesController.class, authenticationMiddleware));

        return router;
    }
}   
