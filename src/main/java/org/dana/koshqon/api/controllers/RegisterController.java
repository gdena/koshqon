package org.dana.koshqon.api.controllers;

import org.dana.koshqon.api.models.requests.RegisterRequest;
import org.dana.koshqon.api.models.responses.UserDto;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.entities.Profile;
import org.dana.koshqon.database.entities.SystemError;
import org.dana.koshqon.database.entities.User;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;
import org.dana.koshqon.rester.utilities.ValidationException;

import java.io.IOException;

public class RegisterController extends HttpController {
    @Override
    protected void doPost() throws ResponseReturned, IOException {
        String requestBody = new String(this.getRequestBody());
        RegisterRequest registerRequest;
        try {
            registerRequest = Json.unmarshal(requestBody, RegisterRequest.class);
        } catch (ValidationException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Profile profile = new Profile(registerRequest);
        User user = null;
        try {
            Database.instance().session().beginTransaction();
            Database.query().transactionalInsert(profile);
            user = new User(registerRequest, profile);
            Database.query().transactionalInsert(user);
            Database.instance().session().getTransaction().commit();
        } catch (Exception err) {
            Database.instance().session().getTransaction().rollback();
            returnResponse(HttpStatusCodes.FORBIDDEN, Json.marshal(SystemError.getError(5)));
        }
        returnResponse(HttpStatusCodes.OK, Json.marshal(UserDto.fromUser(user)));
    }
}
