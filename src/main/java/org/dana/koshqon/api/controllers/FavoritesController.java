package org.dana.koshqon.api.controllers;

import jakarta.persistence.NoResultException;
import org.dana.koshqon.api.models.responses.AnnouncementDto;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.caches.UserCache;
import org.dana.koshqon.database.entities.*;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FavoritesController extends HttpController {
    @Override
    protected void doGet() throws ResponseReturned, IOException {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        Profile profile = user.getProfile();
        returnResponse(HttpStatusCodes.OK, Json.marshal(handleListResult(profile)));
    }

    @Override
    protected void doPost() throws ResponseReturned {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        String announcementIdStr = this.getQueryParam("id");
        if (announcementIdStr == null) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Long announcementId;
        try {
            announcementId = Long.parseLong(announcementIdStr);
        } catch (NumberFormatException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Announcement announcement;
        try {
            announcement = Database.query().select(Announcement.class).equal("id", announcementId).build().getSingleResult();
        } catch (NoResultException e) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        Profile profile = user.getProfile();
        Favorite favorite = new Favorite();
        favorite.setActive(true);
        favorite.setAnnouncement(announcement);
        favorite.setProfile(profile);
        Database.query().insert(favorite);
        returnResponse(HttpStatusCodes.OK, Json.marshal(SystemError.getError(0)));
    }

    @Override
    protected void doDelete() throws ResponseReturned {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        String announcementIdStr = this.getQueryParam("id");
        if (announcementIdStr == null) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Long announcementId;
        try {
            announcementId = Long.parseLong(announcementIdStr);
        } catch (NumberFormatException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Favorite favorite;
        try {
            favorite = Database.query().select(Favorite.class).equal("announcement.id", announcementId).equal("profile.id", user.getProfile().getId()).build().getSingleResult();
        } catch (NoResultException e) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        favorite.setActive(false);
        Database.query().merge(favorite);
        returnResponse(HttpStatusCodes.OK, Json.marshal(SystemError.getError(0)));
    }

    private List<AnnouncementDto> handleListResult(Profile profile) {
        List<Favorite> favorites = null;
        try {
            favorites = Database.query().select(Favorite.class).equal("profile.id", profile.getId()).build().list();
        } catch (NoResultException e) {
            return null;
        }
        List<Announcement> announcements = new ArrayList<>();
        for (Favorite favorite : favorites) {
            announcements.add(favorite.getAnnouncement());
        }

        List<AnnouncementDto> announcementDtos = new ArrayList<>();
        AnnouncementDto announcementDto;
        for (Announcement announcement : announcements) {
            List<byte[]> images = this.getAnnouncementImages(announcement.getId());
            announcementDto = AnnouncementDto.fromAnnouncement(announcement, images);
            announcementDtos.add(announcementDto);
        }
        return announcementDtos;
    }

    private List<byte[]> getAnnouncementImages(Long id) {
        List<AnnouncementImage> announcementImages;
        try {
            announcementImages = Database.query().select(AnnouncementImage.class).equal("announcement.id", id).build().list();
        } catch (NoResultException e) {
            return null;
        }
        List<byte[]> images = new ArrayList<>();
        for (AnnouncementImage announcementImage : announcementImages) {
            images.add(announcementImage.getImageData());
        }
        return images;
    }
}
