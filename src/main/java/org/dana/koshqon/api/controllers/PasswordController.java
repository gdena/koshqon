package org.dana.koshqon.api.controllers;

import jakarta.persistence.NoResultException;
import org.dana.koshqon.api.models.dtos.SecretCode;
import org.dana.koshqon.api.models.requests.PasswordRequest;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.caches.SecretCodeCache;
import org.dana.koshqon.database.entities.SystemError;
import org.dana.koshqon.database.entities.User;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;
import org.dana.koshqon.rester.utilities.ValidationException;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class PasswordController extends HttpController {
    @Override
    protected void doPut() throws ResponseReturned, IOException, NoSuchAlgorithmException {
        String requestBody = new String(this.getRequestBody());
        PasswordRequest passwordRequest = null;
        try {
            passwordRequest = Json.unmarshal(requestBody, PasswordRequest.class);
        } catch (ValidationException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        if (passwordRequest.password() == null || passwordRequest.password().equals("")) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        SecretCode secretCode = SecretCodeCache.getInstance().getSecretCode(passwordRequest.phone());
        if (secretCode == null) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        User user;
        try {
            user = Database.query().select(User.class).equal("phone", secretCode.getPhone_number()).build().getSingleResult();
        } catch (NoResultException e) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        if (user == null || user.getActive().equals(false)) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        user.setSecret(user.getSecretHash(passwordRequest.password()));
        Database.query().merge(user);
        returnResponse(HttpStatusCodes.OK, Json.marshal(SystemError.getError(0)));
    }
}
