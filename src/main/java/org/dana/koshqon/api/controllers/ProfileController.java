package org.dana.koshqon.api.controllers;

import jakarta.persistence.NoResultException;
import org.dana.koshqon.api.models.requests.ProfileEditRequest;
import org.dana.koshqon.api.models.responses.ProfileDto;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.caches.UserCache;
import org.dana.koshqon.database.entities.*;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;
import org.dana.koshqon.rester.utilities.ValidationException;

import java.io.IOException;

public class ProfileController extends HttpController {
    @Override
    protected void doGet() throws ResponseReturned {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        ProfileImage profileImage;
        try {
            profileImage = Database.query().select(ProfileImage.class).equal("profile.id", user.getProfile().getId()).build().getSingleResult();
        } catch (NoResultException e) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        Characteristic characteristic;
        try {
            characteristic = Database.query().select(Characteristic.class).equal("profile.id", user.getProfile().getId()).build().getSingleResult();
        } catch (NoResultException e) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        returnResponse(HttpStatusCodes.OK, Json.marshal(ProfileDto.fromProfile(user.getProfile(), profileImage.getImageData(), characteristic)));
    }

    @Override
    protected void doDelete() throws ResponseReturned {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        user.setActive(false);
        returnResponse(HttpStatusCodes.OK, Json.marshal(SystemError.getError(0)));
    }

    @Override
    protected void doPatch() throws ResponseReturned, IOException {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        String requestBody = new String(this.getRequestBody());
        ProfileEditRequest profileEditRequest;
        try {
            profileEditRequest = Json.unmarshal(requestBody, ProfileEditRequest.class);
        } catch (ValidationException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Profile profile = user.getProfile();
        profile.setFirstName(profileEditRequest.first_name());
        profile.setLastName(profileEditRequest.last_name());
        profile.setBirthdate(profileEditRequest.birthDate());
        profile.setGender(profileEditRequest.gender());
        profile.setCountry(profileEditRequest.country());
        profile.setBiography(profile.getBiography());
        Database.query().merge(profile);

        Characteristic characteristic;
        try {
            characteristic = Database.query().select(Characteristic.class).equal("profile.id", user.getProfile().getId()).build().getSingleResult();
        } catch (NoResultException e) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        characteristic.setAspects(profileEditRequest.aspects());
        characteristic.setInterests(profileEditRequest.interests());
        Database.query().merge(characteristic);
        returnResponse(HttpStatusCodes.OK, Json.marshal(SystemError.getError(0)));
    }
}