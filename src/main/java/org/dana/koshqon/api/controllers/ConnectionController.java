package org.dana.koshqon.api.controllers;

import org.dana.koshqon.api.models.requests.ConnectionRequest;
import org.dana.koshqon.api.models.responses.ConnectionDto;
import org.dana.koshqon.api.models.responses.ProfileDto;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.caches.UserCache;
import org.dana.koshqon.database.entities.*;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;
import org.dana.koshqon.rester.utilities.ValidationException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionController extends HttpController {
    protected void doGet() throws ResponseReturned {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        returnResponse(HttpStatusCodes.OK, Json.marshal(handleListResult(user.getProfile())));
    }

    protected void doPost() throws ResponseReturned, IOException {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        String requestBody = new String(this.getRequestBody());
        ConnectionRequest sendConnectionRequest;
        try {
            sendConnectionRequest = Json.unmarshal(requestBody, ConnectionRequest.class);
        } catch (ValidationException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Profile profile_1 = user.getProfile();
        Profile profile_2 = Database.query().select(Profile.class).equal("id", sendConnectionRequest.profile_id()).build().getSingleResult();
        Connection connection = Connection.getConnection(profile_1.getId(), profile_2.getId());
        if (connection == null) {
            connection = new Connection(profile_1, profile_2, Status.getStatus(3L));
            Database.query().insert(connection);
        }
        connection.setStatus(Status.getStatus(3L));
        Database.query().merge(connection);
        returnResponse(HttpStatusCodes.OK, Json.marshal(ConnectionDto.fromConnection(connection)));
    }

    protected void doPatch() throws ResponseReturned, IOException {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        String requestBody = new String(this.getRequestBody());
        ConnectionRequest connectionRequest;
        try {
            connectionRequest = Json.unmarshal(requestBody, ConnectionRequest.class);
        } catch (ValidationException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Connection connection = Connection.getConnection(user.getProfile().getId(), connectionRequest.profile_id());
        if (connection == null) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        connection.setStatus(Status.getStatus(1L));
        Database.query().merge(connection);
        returnResponse(HttpStatusCodes.OK, Json.marshal(ConnectionDto.fromConnection(connection)));
    }

    protected void doDelete() throws ResponseReturned, IOException {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        String requestBody = new String(this.getRequestBody());
        ConnectionRequest connectionRequest;
        try {
            connectionRequest = Json.unmarshal(requestBody, ConnectionRequest.class);
        } catch (ValidationException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Connection connection = Connection.getConnection(user.getProfile().getId(), connectionRequest.profile_id());
        if (connection == null) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        connection.setStatus(Status.getStatus(2L));
        Database.query().merge(connection);
        returnResponse(HttpStatusCodes.OK, Json.marshal(ConnectionDto.fromConnection(connection)));
    }

    private List<ProfileDto> handleListResult(Profile profile) {
        String queryString = "WHERE profile1 = :profile OR profile2 = :profile";
        List<Connection> connections = Database.query().select(Connection.class).custom(queryString)
                .setParameter("profile", profile).list();
        List<ProfileDto> profiles = new ArrayList<>();
        ProfileImage profileImage1;
        Characteristic characteristic1;
        ProfileImage profileImage2;
        Characteristic characteristic2;
        for (Connection connection : connections) {
            if (connection.getProfile1().getId().equals(profile.getId())) {
                profileImage2 = Database.query().select(ProfileImage.class).equal("profile.id", connection.getProfile2().getId()).build().getSingleResult();
                characteristic2 = Database.query().select(Characteristic.class).equal("profile.id", connection.getProfile2().getId()).build().getSingleResult();
                profiles.add(ProfileDto.fromProfile(connection.getProfile2(), profileImage2.getImageData(), characteristic2));
            }
            if (connection.getProfile2().getId().equals(profile.getId())) {
                profileImage1 = Database.query().select(ProfileImage.class).equal("profile.id", connection.getProfile1().getId()).build().getSingleResult();
                characteristic1 = Database.query().select(Characteristic.class).equal("profile.id", connection.getProfile1().getId()).build().getSingleResult();
                profiles.add(ProfileDto.fromProfile(connection.getProfile1(), profileImage1.getImageData(), characteristic1));
            }
        }
        return profiles;
    }
}
