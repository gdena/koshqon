package org.dana.koshqon.api.controllers;

import org.dana.koshqon.api.models.responses.TokenDto;
import org.dana.koshqon.api.utilities.Jwt;
import org.dana.koshqon.database.caches.UserCache;
import org.dana.koshqon.database.entities.User;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;

import java.util.Date;

public class RefreshAuthController extends HttpController {
    @Override
    protected void doGet() throws ResponseReturned {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        TokenDto authResp = this.getTokens(user);
        returnResponse(HttpStatusCodes.OK, Json.marshal(authResp));
    }

    private TokenDto getTokens(User user) {
        String token = Jwt.getInstance().generateToken(user);
        long nowMillis = System.currentTimeMillis(); // Current time in milliseconds
        long expMillis = nowMillis + (24 * 60 * 60 * 1000); // 24 hours in milliseconds
        Date tokenExp = new Date(expMillis);
        String refreshToken = Jwt.getInstance().generateRefreshToken(user);
        long refreshNowMillis = System.currentTimeMillis(); // Current time in milliseconds
        long refreshExpMillis = refreshNowMillis + (7 * 24 * 60 * 60 * 1000); // 24 hours in milliseconds
        Date refreshTokenExp = new Date(refreshExpMillis);
        TokenDto authResp = new TokenDto(user.getId(), token, refreshToken, tokenExp, refreshTokenExp);
        return authResp;
    }
}
