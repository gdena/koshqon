package org.dana.koshqon.api.controllers;

import jakarta.persistence.NoResultException;
import org.dana.koshqon.api.models.responses.AnnouncementDto;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.entities.Announcement;
import org.dana.koshqon.database.entities.AnnouncementImage;
import org.dana.koshqon.database.entities.SystemError;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DefaultAnnouncementController extends HttpController {
    @Override
    protected void doGet() throws ResponseReturned, IOException {
        String announcementIdStr = this.getQueryParam("id");
        if (announcementIdStr != null) {
            Long id = Long.parseLong(announcementIdStr);
            AnnouncementDto result = handleSingleResult(id);
            if (result == null) {
                returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            }
            returnResponse(HttpStatusCodes.OK, Json.marshal(result));
            return;
        }
        returnResponse(HttpStatusCodes.OK, Json.marshal(handleListResult()));
    }

    private AnnouncementDto handleSingleResult(Long id) {
        Announcement announcement = null;
        try {
            announcement = Database.query().select(Announcement.class).equal("id", id).build().getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        if (announcement == null) {
            return null;
        }
        List<byte[]> images = this.getAnnouncementImages(id);
        return AnnouncementDto.fromAnnouncement(announcement, images);
    }

    private List<AnnouncementDto> handleListResult() {
        List<Announcement> announcements = null;
        try {
            announcements = Database.query().select(Announcement.class).build().list();
        } catch (NoResultException e) {
            return null;
        }
        List<AnnouncementDto> announcementDtos = new ArrayList<AnnouncementDto>();
        AnnouncementDto announcementDto;
        for (Announcement announcement : announcements) {
            List<byte[]> images = this.getAnnouncementImages(announcement.getId());
            announcementDto = AnnouncementDto.fromAnnouncement(announcement, images);
            announcementDtos.add(announcementDto);
        }
        return announcementDtos;
    }

    private List<byte[]> getAnnouncementImages(Long id) {
        List<AnnouncementImage> announcementImages;
        try {
            announcementImages = Database.query().select(AnnouncementImage.class).equal("announcement.id", id).build().list();
        } catch (NoResultException e) {
            return null;
        }
        List<byte[]> images = new ArrayList<>();
        for (AnnouncementImage announcementImage : announcementImages) {
            images.add(announcementImage.getImageData());
        }
        return images;
    }
}
