package org.dana.koshqon.api.controllers;

import jakarta.persistence.NoResultException;
import org.dana.koshqon.api.models.responses.ProfileDto;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.entities.Characteristic;
import org.dana.koshqon.database.entities.Profile;
import org.dana.koshqon.database.entities.ProfileImage;
import org.dana.koshqon.database.entities.SystemError;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DefaultProfileController extends HttpController {
    @Override
    protected void doGet() throws ResponseReturned, IOException {
        String profileIdStr = this.getQueryParam("id");
        if (profileIdStr != null) {
            Long profileId = Long.parseLong(profileIdStr);
            ProfileDto result = handleSingleResult(profileId);
            if (result == null) {
                returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            }
            returnResponse(HttpStatusCodes.OK, Json.marshal(result));
            return;
        }
        returnResponse(HttpStatusCodes.OK, Json.marshal(handleListResult()));
    }

    private ProfileDto handleSingleResult(Long id) {
        Profile profile = null;
        try {
            profile = Database.query().select(Profile.class).equal("id", id).build().getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        if (profile == null) {
            return null;
        }
        byte[] image = this.getProfileImage(id);
        Characteristic characteristic = this.getProfileCharacteristic(id);
        return ProfileDto.fromProfile(profile, image, characteristic);
    }

    private List<ProfileDto> handleListResult() {
        List<Profile> profiles = null;
        try {
            profiles = Database.query().select(Profile.class).build().list();
        } catch (NoResultException e) {
            return null;
        }
        List<ProfileDto> profileDtos = new ArrayList<ProfileDto>();
        ProfileDto profileDto;
        Characteristic characteristic;
        for (Profile profile : profiles) {
            byte[] image = this.getProfileImage(profile.getId());
            characteristic = this.getProfileCharacteristic(profile.getId());
            profileDto = ProfileDto.fromProfile(profile, image, characteristic);
            profileDtos.add(profileDto);
        }
        return profileDtos;
    }

    private byte[] getProfileImage(Long id) {
        ProfileImage profileImages;
        try {
            profileImages = Database.query().select(ProfileImage.class).equal("profile.id", id).build().getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return profileImages.getImageData();
    }

    private Characteristic getProfileCharacteristic(Long id) {
        Characteristic characteristic;
        try {
            characteristic = Database.query().select(Characteristic.class).equal("profile.id", id).build().getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return characteristic;
    }
}
