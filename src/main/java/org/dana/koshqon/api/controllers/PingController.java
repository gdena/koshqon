package org.dana.koshqon.api.controllers;

import org.dana.koshqon.api.models.responses.Message;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;
import org.hibernate.Session;

public class PingController extends HttpController {
    @Override
    protected void doGet() throws ResponseReturned {
        Session session = Database.instance().session();
        session.createQuery("SELECT 1").list();
        session.close();
        returnResponse(HttpStatusCodes.OK, Json.marshal(new Message("PONG")));
    }
}
