package org.dana.koshqon.api.controllers;

import org.dana.koshqon.api.models.dtos.SecretCode;
import org.dana.koshqon.api.models.requests.PhoneVerificationRequest;
import org.dana.koshqon.api.models.requests.VerificationCodeRequest;
import org.dana.koshqon.database.caches.SecretCodeCache;
import org.dana.koshqon.database.entities.SystemError;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;
import org.dana.koshqon.rester.utilities.ValidationException;

import java.io.IOException;
import java.security.SecureRandom;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

public class PhoneVerificationController extends HttpController {
    @Override
    protected void doPost() throws ResponseReturned, IOException {
        String requestBody = new String(this.getRequestBody());
        PhoneVerificationRequest phoneVerificationRequest = null;
        try {
            phoneVerificationRequest = Json.unmarshal(requestBody, PhoneVerificationRequest.class);
        } catch (ValidationException e) {
            returnResponse(HttpStatusCodes.FORBIDDEN, Json.marshal(SystemError.getError(2)));
            return;
        }
        if (phoneVerificationRequest.phone() == null || phoneVerificationRequest.phone().equals("")) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        if (SecretCodeCache.getInstance().getSecretCode(phoneVerificationRequest.phone()) != null) {
            returnResponse(HttpStatusCodes.PRECONDITION_FAILED, Json.marshal(SystemError.getError(4)));
        }
//        String secret = generateRandom6DigitCode();
        String secret = "123456";
        SecretCode secretCode = new SecretCode();
        secretCode.setPhone_number(phoneVerificationRequest.phone());
        secretCode.setStatus(0);
        secretCode.setSecret(secret);
        secretCode.setCreated_at();
        SecretCodeCache.getInstance().setSecretCode(secretCode);
        // send message
        returnResponse(HttpStatusCodes.OK, Json.marshal(SystemError.getError(0)));
    }

    @Override
    protected void doPatch() throws ResponseReturned, IOException {
        String requestBody = new String(this.getRequestBody());
        VerificationCodeRequest verificationCodeRequest = null;
        try {
            verificationCodeRequest = Json.unmarshal(requestBody, VerificationCodeRequest.class);
        } catch (ValidationException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        if (verificationCodeRequest == null) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        SecretCode secretCode = SecretCodeCache.getInstance().getSecretCode(verificationCodeRequest.phone());
        if (secretCode == null) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        if (!validateSecretCodeDate(secretCode)) {
            returnResponse(HttpStatusCodes.UNAUTHORIZED, Json.marshal(SystemError.getError(1)));
        }
        if (!secretCode.getSecret().equals(verificationCodeRequest.secret())) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
        }
        secretCode.setStatus(1);
        SecretCodeCache.getInstance().setSecretCode(secretCode);
        returnResponse(HttpStatusCodes.OK, Json.marshal(SystemError.getError(0)));
    }

    private String generateRandom6DigitCode() {
        SecureRandom secureRandom = new SecureRandom();
        int code = 100_000 + secureRandom.nextInt(900_000);
        return String.valueOf(code);
    }

    private boolean validateSecretCodeDate(SecretCode secretCode) {
        OffsetDateTime currentTime = OffsetDateTime.now();
        long maxAllowedSeconds = 120;
        long minutesDifference = ChronoUnit.SECONDS.between(secretCode.getCreated_at(), currentTime);
        return minutesDifference <= maxAllowedSeconds;
    }
}
