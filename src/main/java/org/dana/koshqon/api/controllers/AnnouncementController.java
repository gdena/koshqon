package org.dana.koshqon.api.controllers;

import jakarta.persistence.NoResultException;
import org.dana.koshqon.api.models.requests.AnnouncementRequest;
import org.dana.koshqon.api.models.responses.AnnouncementDto;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.caches.UserCache;
import org.dana.koshqon.database.entities.*;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;
import org.dana.koshqon.rester.utilities.ValidationException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AnnouncementController extends HttpController {
    @Override
    protected void doGet() throws ResponseReturned, IOException {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        String announcementIdStr = this.getQueryParam("id");
        Profile profile = user.getProfile();
        if (announcementIdStr != null) {
            Long announcementId = Long.parseLong(announcementIdStr);
            AnnouncementDto result = handleSingleResult(announcementId, profile);
            if (result == null) {
                returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            }
            returnResponse(HttpStatusCodes.OK, Json.marshal(result));
            return;
        }
        returnResponse(HttpStatusCodes.OK, Json.marshal(handleListResult(profile)));
    }

    @Override
    protected void doPost() throws ResponseReturned, IOException {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        String requestBody = new String(this.getRequestBody());
        AnnouncementRequest announcementRequest;
        try {
            announcementRequest = Json.unmarshal(requestBody, AnnouncementRequest.class);
        } catch (ValidationException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Profile profile = user.getProfile();
        Announcement announcement = new Announcement();
        announcement.setCity(announcementRequest.city());
        announcement.setCountry(announcementRequest.country());
        announcement.setAddress(announcementRequest.address());
        announcement.setAnnouncementType(AnnouncementType.getAnnouncementType(announcementRequest.announcementTypeId()));
        announcement.setDescription(announcement.getDescription());
        announcement.setProfile(profile);
        announcement.setActive(true);
        Database.query().insert(announcement);

        for (byte[] image : announcementRequest.images()) {
            AnnouncementImage announcementImage = new AnnouncementImage();
            announcementImage.setImageData(image);
            announcementImage.setAnnouncement(announcement);
            Database.query().insert(announcementImage);
        }

        returnResponse(HttpStatusCodes.OK, Json.marshal(AnnouncementDto.fromAnnouncement(announcement, announcementRequest.images())));
    }

    @Override
    protected void doDelete() throws ResponseReturned {
        User user = UserCache.getInstance().getUser(getResponseHeader("X-USER-ID"));
        String announcementIdStr = this.getQueryParam("id");
        if (announcementIdStr == null) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Long announcementId;
        try {
            announcementId = Long.parseLong(announcementIdStr);
        } catch (NumberFormatException e) {
            returnResponse(HttpStatusCodes.BAD_REQUEST, Json.marshal(SystemError.getError(2)));
            return;
        }
        Announcement announcement;
        try {
            announcement = Database.query().select(Announcement.class).equal("id", announcementId).build().getSingleResult();
        } catch (NoResultException e) {
            returnResponse(HttpStatusCodes.NOT_FOUND, Json.marshal(SystemError.getError(3)));
            return;
        }
        announcement.setActive(false);
        Database.query().merge(announcement);
        returnResponse(HttpStatusCodes.OK, Json.marshal(SystemError.getError(0)));
    }

    private AnnouncementDto handleSingleResult(Long announcementId, Profile profile) {
        Announcement announcement = null;
        try {
            announcement = Database.query().select(Announcement.class).equal("id", announcementId).equal("profile.id", profile.getId()).build().getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        if (announcement == null) {
            return null;
        }
        List<byte[]> images = this.getAnnouncementImages(announcementId);
        return AnnouncementDto.fromAnnouncement(announcement, images);
    }

    private List<AnnouncementDto> handleListResult(Profile profile) {
        List<Announcement> announcements = null;
        try {
            announcements = Database.query().select(Announcement.class).equal("profile.id", profile.getId()).build().list();
        } catch (NoResultException e) {
            return null;
        }
        List<AnnouncementDto> announcementDtos = new ArrayList<AnnouncementDto>();
        AnnouncementDto announcementDto;
        for (Announcement announcement : announcements) {
            List<byte[]> images = this.getAnnouncementImages(announcement.getId());
            announcementDto = AnnouncementDto.fromAnnouncement(announcement, images);
            announcementDtos.add(announcementDto);
        }
        return announcementDtos;
    }

    private List<byte[]> getAnnouncementImages(Long id) {
        List<AnnouncementImage> announcementImages;
        try {
            announcementImages = Database.query().select(AnnouncementImage.class).equal("announcement.id", id).build().list();
        } catch (NoResultException e) {
            return null;
        }
        List<byte[]> images = new ArrayList<>();
        for (AnnouncementImage announcementImage : announcementImages) {
            images.add(announcementImage.getImageData());
        }
        return images;
    }
}
