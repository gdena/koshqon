package org.dana.koshqon.api.controllers;

import jakarta.persistence.NoResultException;
import org.dana.koshqon.api.models.responses.TokenDto;
import org.dana.koshqon.api.utilities.Jwt;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.entities.SystemError;
import org.dana.koshqon.database.entities.User;
import org.dana.koshqon.rester.HttpController;
import org.dana.koshqon.rester.http.HttpStatusCodes;
import org.dana.koshqon.rester.response.models.ResponseReturned;
import org.dana.koshqon.rester.utilities.Json;

import java.util.Base64;
import java.util.Date;

public class AuthController extends HttpController {
    User user;

    @Override
    protected void doGet() throws ResponseReturned {
        boolean validUser = false;
        try {
            validUser = this.checkAuthentication();
        } catch (Exception ignored) {
        }
        if (!validUser) {
            returnResponse(HttpStatusCodes.UNAUTHORIZED, Json.marshal(SystemError.getError(1)));
        }
        TokenDto authResp = getTokens(user);
        returnResponse(HttpStatusCodes.OK, Json.marshal(authResp));
    }

    private TokenDto getTokens(User user) {
        String token = Jwt.getInstance().generateToken(user);
        long nowMillis = System.currentTimeMillis(); // Current time in milliseconds
        long expMillis = nowMillis + (24 * 60 * 60 * 1000); // 24 hours in milliseconds
        Date tokenExp = new Date(expMillis);
        String refreshToken = Jwt.getInstance().generateRefreshToken(user);
        long refreshNowMillis = System.currentTimeMillis(); // Current time in milliseconds
        long refreshExpMillis = refreshNowMillis + (7 * 24 * 60 * 60 * 1000); // 24 hours in milliseconds
        Date refreshTokenExp = new Date(refreshExpMillis);
        TokenDto authResp = new TokenDto(user.getId(), token, refreshToken, tokenExp, refreshTokenExp);
        return authResp;
    }

    private boolean checkAuthentication() {
        String authHeader = exchange.getRequestHeaders().getFirst("Authorization");
        String encodedCredentials = authHeader.split(" ")[1];
        String decodedCredentials = new String(Base64.getDecoder().decode(encodedCredentials));
        String[] credentials = decodedCredentials.split(":");
        String username = credentials[0];
        String password = credentials[1];
        User user;
        try {
            user = Database.query().select(User.class).equal("login", username).build().getSingleResult();
        } catch (NoResultException err) {
            return false;
        }
        if (user == null || user.getActive().equals(false)) {
            return false;
        }
        if (!user.validatePassword(password)) {
            return false;
        }
        this.user = user;
        return true;
    }
}
