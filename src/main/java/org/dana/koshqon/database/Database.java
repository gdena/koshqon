package org.dana.koshqon.database;

import org.dana.koshqon.database.entities.*;
import org.dana.koshqon.rester.dataaccess.PostgresSQLDB;
import org.dana.koshqon.rester.dataaccess.query.QuerySet;
import org.hibernate.Session;

import java.sql.Connection;

public class Database {
    private static Database instance;
    private final PostgresSQLDB connection;

    private Database(String url, String username, String password) {
        connection = new PostgresSQLDB(url, username, password);
        this.registerClasses();
    }

    public static Database instance() {
        if (instance == null) {
            instance = new Database(
                    System.getenv("DB_URL"),
                    System.getenv("DB_USER"),
                    System.getenv("DB_PASSWORD")
            );
        }
        return instance;
    }

    public static QuerySet query() {
        return Database.instance().connection.querySet();
    }

    private void registerClasses() {
        Class[] classes = {
                User.class,
                Characteristic.class,
                Connection.class,
                Profile.class,
                Status.class,
                Announcement.class,
                AnnouncementType.class,
                AnnouncementImage.class,
                ProfileImage.class,
                Favorite.class
        };
        connection.addAnnotatedClass(classes);
    }

    public Session session() {
        return connection.getSession();
    }
}
