package org.dana.koshqon.database.entities;

import jakarta.persistence.*;
import org.dana.koshqon.api.models.requests.RegisterRequest;
import org.dana.koshqon.rester.utilities.ErrorHandler;
import org.dana.koshqon.rester.utilities.HexadecimalConverter;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "login", nullable = false, length = Integer.MAX_VALUE)
    private String login;

    @Column(name = "password", nullable = false, length = Integer.MAX_VALUE)
    private String password;

    @Column(name = "phone", length = Integer.MAX_VALUE)
    private String phone;

    @Column(name = "active", nullable = false)
    private Boolean active = false;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "profile_id")
    private Profile profile;

    public User() {
        this.login = null;
        this.password = null;
        this.phone = null;
        this.active = null;
    }

    public User(RegisterRequest registerRequest, Profile profile) {
        this.login = registerRequest.login();
        this.password = registerRequest.password();
        this.phone = registerRequest.phone();
        this.active = true;
        this.profile = profile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSecret() {
        return password;
    }

    public void setSecret(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public boolean validatePassword(String password) {
        byte[] inputBytes = password.getBytes(StandardCharsets.UTF_8);
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA3-384");
            return HexadecimalConverter.encode(digest.digest(inputBytes)).equals(this.password);
        } catch (NoSuchAlgorithmException err) {
            ErrorHandler.logError(err);
            return false;
        }
    }

    public String getSecretHash(String password) throws NoSuchAlgorithmException {
        byte[] inputBytes = password.getBytes(StandardCharsets.UTF_8);
        MessageDigest digest = MessageDigest.getInstance("SHA3-384");
        return HexadecimalConverter.encode(digest.digest(inputBytes));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User that = (User) o;
        return getId() != null && getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}