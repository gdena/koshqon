package org.dana.koshqon.database.entities;

import jakarta.persistence.*;
import org.dana.koshqon.database.Database;

@Entity
@Table(name = "connections")
public class Connection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "profile_1")
    private Profile profile1;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "profile_2")
    private Profile profile2;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "status")
    private Status status;

    public Connection() {
        this.profile1 = null;
        this.profile2 = null;
        this.status = null;
    }

    public Connection(Profile profile1, Profile profile2, Status status) {
        this.profile1 = profile1;
        this.profile2 = profile2;
        this.status = status;
    }

    public static Connection getConnection(Long profile_1, Long profile_2) {
        String queryString = "WHERE (profile1.id = :profile_1 OR profile2.id = :profile_1) AND (profile1.id = :profile_2 OR profile2.id = :profile_2)";
        Connection connection;
        try {
            connection = Database.query().select(Connection.class).custom(queryString)
                    .setParameter("profile_1", profile_1)
                    .setParameter("profile_2", profile_2)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return connection;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Profile getProfile1() {
        return profile1;
    }

    public void setProfile1(Profile profile1) {
        this.profile1 = profile1;
    }

    public Profile getProfile2() {
        return profile2;
    }

    public void setProfile2(Profile profile2) {
        this.profile2 = profile2;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User that = (User) o;
        return getId() != null && getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
