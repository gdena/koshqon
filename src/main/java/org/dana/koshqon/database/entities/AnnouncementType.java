package org.dana.koshqon.database.entities;

import jakarta.persistence.*;
import org.dana.koshqon.database.Database;

@Entity
@Table(name = "announcement_type")
public class AnnouncementType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;

    public static AnnouncementType getAnnouncementType(Long id) {
        AnnouncementType announcementType;
        try {
            announcementType = Database.query().select(AnnouncementType.class).equal("id", id).build().getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return announcementType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
