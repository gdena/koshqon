package org.dana.koshqon.database.entities;

import jakarta.persistence.*;
import org.dana.koshqon.api.models.responses.ErrorDto;
import org.dana.koshqon.database.caches.ErrorCache;

@Entity
@Table(name = "errors")
public class SystemError {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "error_code", nullable = false)
    private int errorCode;

    @Column(name = "name")
    private String name;

    @Column(name = "description ")
    private String description;

    public SystemError() {
    }

    public SystemError(int errorCode, String name, String description) {
        this.errorCode = errorCode;
        this.name = name;
        this.description = description;
    }

    public static ErrorDto getError(Integer id) {
        return ErrorDto.fromError(ErrorCache.getInstance().getTransactionError(id));
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int id) {
        this.errorCode = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
