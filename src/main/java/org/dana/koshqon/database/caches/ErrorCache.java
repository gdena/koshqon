package org.dana.koshqon.database.caches;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.entities.SystemError;

import java.util.concurrent.TimeUnit;

public class ErrorCache {
    private static ErrorCache instance;
    private final Cache<Integer, SystemError> cache;

    private ErrorCache() {
        cache = CacheBuilder.newBuilder()
                .expireAfterWrite(24, TimeUnit.HOURS)
                .build();
    }

    public static ErrorCache getInstance() {
        if (instance == null) {
            instance = new ErrorCache();
        }
        return instance;
    }

    public SystemError getTransactionError(Integer transactionErrorId) {
        SystemError transactionSystemError = this.cache.getIfPresent(transactionErrorId);
        if (transactionSystemError == null) {
            transactionSystemError = Database.query().select(SystemError.class).equal("errorCode", transactionErrorId).build().getSingleResult();
            this.cache.put(transactionErrorId, transactionSystemError);
        }
        return transactionSystemError;
    }
}
