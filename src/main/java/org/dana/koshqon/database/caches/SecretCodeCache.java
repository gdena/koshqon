package org.dana.koshqon.database.caches;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.dana.koshqon.api.models.dtos.SecretCode;

import java.util.concurrent.TimeUnit;

public class SecretCodeCache {
    private static SecretCodeCache instance;
    private final Cache<String, SecretCode> cache;

    private SecretCodeCache() {
        cache = CacheBuilder.newBuilder()
                .expireAfterWrite(2, TimeUnit.MINUTES)
                .build();
    }

    public static SecretCodeCache getInstance() {
        if (instance == null) {
            instance = new SecretCodeCache();
        }
        return instance;
    }

    public SecretCode getSecretCode(String phone_number) {
        return this.cache.getIfPresent(phone_number);
    }

    public void setSecretCode(SecretCode secretCode) {
        this.cache.put(secretCode.getPhone_number(), secretCode);
    }
}
