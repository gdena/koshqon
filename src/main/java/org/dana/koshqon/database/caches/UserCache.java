package org.dana.koshqon.database.caches;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.dana.koshqon.database.Database;
import org.dana.koshqon.database.entities.User;

import java.util.concurrent.TimeUnit;

public class UserCache {
    private static UserCache instance;
    private final Cache<Long, User> cache;

    private UserCache() {
        cache = CacheBuilder.newBuilder()
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build();
    }

    public static UserCache getInstance() {
        if (instance == null) {
            instance = new UserCache();
        }
        return instance;
    }

    public void write(Long merchantId, User user) {
        this.cache.put(merchantId, user);
    }

    public User getUser(Long userId) {
        User user = this.cache.getIfPresent(userId);
        if (user == null) {
            user = Database.query().select(User.class).equal("id", userId).build().getSingleResult();
            this.cache.put(userId, user);
        }
        return user;
    }

    public User getUser(String userIdStr) {
        Long uid = Long.parseLong(userIdStr);
        return getUser(uid);
    }
}
