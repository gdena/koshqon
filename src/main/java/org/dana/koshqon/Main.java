package org.dana.koshqon;

import org.dana.koshqon.api.routers.RouterFactory;
import org.dana.koshqon.rester.Server;
import org.dana.koshqon.rester.logger.LogWriter;

public class Main {
    public static void main(String[] args) {
        LogWriter logWriter = LogWriter.getInstance();
        Thread logger = new Thread(logWriter);
        logger.start();


        Server server = new Server(8080, RouterFactory.buildRouter());
        server.run();

        System.out.println("KoshQon service runs");
    }
}