FROM ibm-semeru-runtimes:open-17-jre-focal
#FROM eclipse-temurin:17-jre
COPY /target/KoshQon-1.0-SNAPSHOT.jar /app/
CMD ["java", "-jar", "/app/KoshQon-1.0-SNAPSHOT.jar"]
